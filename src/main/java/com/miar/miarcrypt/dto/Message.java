package com.miar.miarcrypt.dto;

import java.util.Date;

//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;

//@Entity
public class Message {

//  @Id
//  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
  private String textToCrypt;
  private String message;
  private String key;
  private String crypted;
  
  private String code;
  private Date creationTime;
  private Long idUser;
  private byte[] messagebytes;
  private Byte[] idbytes;
  
  
  


  
  //protected Message(){}
  public Message(){}
  

  public Message(String textToCrypt, String key, String crypted, String message) {
      super();
      this.textToCrypt = textToCrypt;
      this.key = key;
      this.crypted = crypted;
      this.message = message;
      
  }


  
  
public String getMessage() {
	return message;
}


public void setMessage(String message) {
	this.message = message;
}


public Long getId() {
	return id;
}


public void setId(Long id) {
	this.id = id;
}


public String getTextToCrypt() {
	return textToCrypt;
}


public void setTextToCrypt(String textToCrypt) {
	this.textToCrypt = textToCrypt;
}


public String getKey() {
	return key;
}


public void setKey(String key) {
	this.key = key;
}


public String getCrypted() {
	return crypted;
}


public void setCrypted(String crypted) {
	this.crypted = crypted;
};
  
  
  
  
}