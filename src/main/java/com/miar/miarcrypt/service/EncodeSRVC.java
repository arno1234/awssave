package com.miar.miarcrypt.service;

import java.security.Provider;
import java.security.spec.KeySpec;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
//import org.hibernate.Query;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;

import com.miar.miarcrypt.dto.Message;

//TODO : Check how to manage Authentication
//import com.miar.miarcrypt.common.util.HibernateUtil;
//import com.miar.miarcrypt.domain.Message;
//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

public class EncodeSRVC {

	   /*
	    * 	AES/CBC/NoPadding (128) 
			AES/CBC/PKCS5Padding (128) 
			AES/ECB/NoPadding (128) 
			AES/ECB/PKCS5Padding (128) 
			DES/CBC/NoPadding (56) 
			DES/CBC/PKCS5Padding (56) 
			DES/ECB/NoPadding (56) 
			DES/ECB/PKCS5Padding (56) 
			DESede/CBC/NoPadding (168) 
			DESede/CBC/PKCS5Padding (168) 
			DESede/ECB/NoPadding (168) 
			DESede/ECB/PKCS5Padding (168) 
			RSA/ECB/PKCS1Padding (1024, 2048) 
			RSA/ECB/OAEPWithSHA-1AndMGF1Padding (1024, 2048) 
			RSA/ECB/OAEPWithSHA-256AndMGF1Padding (1024, 2048) 

	    */
	   private static final String UNICODE_FORMAT = "UTF-8";
	   public static final String ENCRYPTION_SCHEME = "DES";
	   //public static final String ENCRYPTION_SCHEME = "AES";
	
	   private KeySpec miarKeySpec;
	   private SecretKeyFactory miarSecretKeyFactory;
	   private Cipher cipher;
	   byte[] miarEncryptionKeyAsBytes;
	   private String miarEncryptionKey;
	   private String miarEncryptionScheme;
	   SecretKey miarSecretKey;
	   Provider p;
	   long idUser;
	
	   Log logger = LogFactory.getLog(this.getClass());
	   
	   // logger.debug
	   // System.out.println
		
		
	   /**
	    * Constructor. Initiates all datas to crypt.
	    * @param pKey : 
	    * @param idLoggedUser : 
	    * @throws Exception
	    */
	   public EncodeSRVC(String pKey, long pIdLoggedUser) throws Exception
	   {
		   // User connected.
		   // Maybe user is not connected, so we just initiate with O.
		   System.out.println("pidLoggedUser : " + pIdLoggedUser);
		   System.out.println("pKey : " + pKey);
		   
		   idUser = pIdLoggedUser;

		   
		   // Check if user's key if ok to Crypt (8 Chars ?) or completes it.
		   miarEncryptionKey = createKey(pKey);
		   //miarEncryptionKey = "12345678";
		   miarEncryptionKeyAsBytes = miarEncryptionKey.getBytes();
		   
		   System.out.println("miarEncryptionKey : " + miarEncryptionKey.toString());
		   
		   
	       // Prepare/instanciate attributes for Crypting
	       miarSecretKey = new SecretKeySpec(miarEncryptionKeyAsBytes, ENCRYPTION_SCHEME);
	       System.out.println("miarSecretKey : " + miarSecretKey.toString());
	       
	       // Instanciate Cipher
	       cipher = Cipher.getInstance(ENCRYPTION_SCHEME);
	       
	       // debug logging
	       System.out.println("idLoggedUser : " + pIdLoggedUser);
		   System.out.println("miarEncryptionKey : " + miarEncryptionKey.toString());
		   System.out.println("miarEncryptionKeyAsBytes : " + miarEncryptionKeyAsBytes.toString());
		   System.out.println("miarEncryptionKeyAsBytes length : " + miarEncryptionKeyAsBytes.length);
//		   System.out.println("cipher algo : " + cipher.getAlgorithm());
//	       System.out.println("cipher block size : " + cipher.getBlockSize());
//	       System.out.println("cipher provider : " + (cipher.getProvider()).getName());
	       
	   }

	   /**
	    * Method To Encrypt The String.
	    */
	   //public String encrypt(String pStringToEncrypt) {
	   public byte[] encrypt(String pStringToEncrypt) {
	
		   byte[] bytesToEncrypt = null;
		   byte[] encryptedBytes = null;
		   
		   try {
			   
			   System.out.println("pStringToEncrypt : " + pStringToEncrypt);
			   System.out.println("miarSecretKey : " + miarSecretKey);


		       // init Cipher in ENCRYPTION mode
	           // Crypt with Cipher 
	           cipher.init(Cipher.ENCRYPT_MODE, miarSecretKey);
			   System.out.println("End cipher.init : " + cipher.toString());
			   
			   bytesToEncrypt = pStringToEncrypt.getBytes("UTF-8") ;//getBytes()
	           System.out.println("bytesToEncrypt UTF-8 : " + bytesToEncrypt);
	           
	           encryptedBytes = cipher.doFinal(bytesToEncrypt);
	           System.out.println("encryptedBytes : " + encryptedBytes.toString());
	           System.out.println("encryptedBytes length : " + encryptedBytes.length);
	           //System.out.println("encryptedString :" + bytes2String(encryptedBytes));
	           //System.out.println("encryptedString constructeur :" + new String(encryptedBytes));
		       
			    //return messageCode;
	           // RETURN encrypted String.
	           System.out.println("Encrypted String Returned : "+ encryptedBytes.toString());
	           return encryptedBytes;

	        		   
	       } catch (Exception e) {
	    	   
	    	   logger.error("ERREUR encrypt");
	           e.printStackTrace();
	       }
	       
	       //return encryptedString;
		   return null;
	   }
	   
	   
	   
	   /**
	    * Method To Decrypt A Crypted String
	    * Test : [B@2fc14f68 / Key : miarcryptkey
	    * @param pkey : Key to decrypt message
	    * @param pcode : Encrypted string to decode
	    */
	   //public String decrypt(byte[] encryptedBytes) {
	   //public byte[] decrypt(String encryptedString) {
	   //public String decrypt(String pkey, String pcode ) {
	   public String decrypt(String pkey, byte[] pEncryptedBytes ) {
	       //String decryptedText=null;
		   //byte[] bytesToDecrypt= encryptedBytes;
		   byte[] bytesToDecrypt=null;
		   byte[] decryptedBytes = null;
	       
		   // TODO : Get data from DB to decrypt
	       try {
	    	   
	    	   
		       // Init CIpher to decode Message           
	           cipher.init(Cipher.DECRYPT_MODE, miarSecretKey);
	           //bytesToDecrypt = message.getMessage();
	           //bytesToDecrypt = pcode.getBytes();
	           
	           //bytesToDecrypt = pcode.getBytes("UTF-8");
	           
	           System.out.println("bytesToDecrypt : " + pEncryptedBytes.toString());
	           System.out.println("bytesToDecrypt length : " + pEncryptedBytes.length);
	           
	           // decryptedBytes = cipher.doFinal(bytesToDecrypt);
	           decryptedBytes = cipher.doFinal(pEncryptedBytes);
	           //decryptedText= bytes2String(plainText);
	           
	           System.out.println("bytesDecrypted : " + decryptedBytes);
	           System.out.println("bytesDecrypted length : " + decryptedBytes.length);
	           
	       } catch (Exception e) {
	    	   System.out.println("ERREUR decrypt");
	           e.printStackTrace();
	       }
	      
	       return new String(decryptedBytes) ;
	   }
	       
	   /**
	    * Returns String From An Array Of Bytes
	    */
	   private static String bytes2String(byte[] bytes) {
	       StringBuffer stringBuffer = new StringBuffer();
	       for (int i = 0; i < bytes.length; i++) {
	           stringBuffer.append((char) bytes[i]);
	       }
	       return stringBuffer.toString();
	   }

	   
	   /**
	    * Create key to encrypt.
	    * Key has to be 8 chars long.
	    * @param pKey
	    * @return
	    */
	   private String createKey(String pKey){
		   
		   // TODO : Check if key has to be 8 chars long ?
		   System.out.println("createKey pKey : " + pKey);
		   
		   
		   StringBuffer returnKey = new StringBuffer();
		   
		   // check if the parameter is ok
		   String key = pKey==null ? "" : pKey;
		   returnKey.append(key);
		   
		   int keyLenth = returnKey.length();
		   // complete the parameter to have a key of 8 chars long
		   //for (int i = 1; i <= 16 - keyLenth; i++)
		   for (int i = 1; i <= 8 - keyLenth; i++)
		   {
			   returnKey.append(10-i);
		   }
		   
		   System.out.println("ReturnedKey : " + returnKey.toString());
		   return returnKey.toString();

	   }
	   
	   /**
	    * Testing the DES Encryption And Decryption <span id="IL_AD2" class="IL_AD">Technique</span>
	    
	   public static void main(String args []) throws Exception
	   {
	       DESEncryption myEncryptor= new DESEncryption();

	       String stringToEncrypt="Sanjaal.com";
	       String encrypted=myEncryptor.encrypt(stringToEncrypt);
	       String decrypted=myEncryptor.decrypt(encrypted);

	       System.out.println("String To Encrypt: "+stringToEncrypt);
	       System.out.println("Encrypted Value :" + encrypted);
	       System.out.println("Decrypted Value :"+decrypted);

	   }  
	 
	 */
	   
	   
	   // TEST	       
/*       
	       //KeyGenerator keygenerator = KeyGenerator.getInstance("DES");
		   //SecretKey myDesKey = keygenerator.generateKey();
		   byte[] badEncoded = "Key98123".getBytes() ; //Key data to Test bad key
		   SecretKey badSecretKey = new SecretKeySpec(badEncoded, "DES");	// 	Bad Secret Key for an algorithm
		   
		   byte[] encoded = "Key98765".getBytes() ; 						//	Key data in bytes
		   SecretKey secretKey = new SecretKeySpec(encoded, "DES");			// 	Secret Key for an algorithm

		   Cipher desCipher;

		   // Create the cipher 
		   //desCipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		   desCipher = Cipher.getInstance("DES");							// Cipher for the algorithm
		   
		   // Initialize the cipher for encryption
		   //desCipher.init(Cipher.ENCRYPT_MODE, myDesKey);
		   desCipher.init(Cipher.ENCRYPT_MODE, secretKey);					// Init cipher to encode with SecretKey
		   
		   //sensitive information
		    byte[] text = "No body can see me me".getBytes();				// Text to encode, in bytes

		    logger.debug("Text [Byte Format] : " + text);
		    logger.debug("Text : " + new String(text));

		    // Encrypt the text
		    byte[] textEncrypted = desCipher.doFinal(text);					// Encode text, in bytes.

		    logger.debug("Text Encryted : " + textEncrypted);
		    logger.debug("Text Encryted Bytes length : " + textEncrypted.length);
		    // Initialize the same cipher for decryption
		    //desCipher.init(Cipher.DECRYPT_MODE, myDesKey);
		    desCipher.init(Cipher.DECRYPT_MODE, secretKey);					// re-init cipher to decode	
		    
		    // Decrypt the text
		    byte[] textDecrypted = desCipher.doFinal(textEncrypted);		// decode the text, in bytes
		    
		    logger.debug("Text Decryted : " + new String(textDecrypted));
		    
		    // Decrypt the text
		    Cipher newCipher = Cipher.getInstance("DES");	
		    newCipher.init(Cipher.DECRYPT_MODE, secretKey);					// re-init cipher to decode	with bad secret key
		    byte[] textNewDecrypted = newCipher.doFinal(textEncrypted);		// decode the text, in bytes
		    
		    logger.debug("Text NEW Decryted : " + new String(textNewDecrypted));
		    
		    // Decrypt the text
		    
		    Cipher badCipher = Cipher.getInstance("DES");	
		    logger.debug("1");
		    badCipher.init(Cipher.DECRYPT_MODE, badSecretKey);					// re-init cipher to decode	with bad secret key
		    logger.debug("2");
		    byte[] textBadDecrypted = badCipher.doFinal(textEncrypted);		// decode the text, in bytes
		    logger.debug("3");
		    
		    logger.debug("Text BAD Decryted : " + new String(textBadDecrypted));
			
*/
	    // END TEST

	   

	   
	   
}