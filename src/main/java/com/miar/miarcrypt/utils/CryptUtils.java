package com.miar.miarcrypt.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class CryptUtils {

	
	// TODO : manage instanciation phase
	public CryptUtils() {
		// Initialize Class
	}


	/**
	 * Encode numerical  ID with letters and Numbers
	 * @param _messageid : id to encode, on numbers.
	 * @return : id with numbers and letters.
	 */
	public static String idencoding(String _messageid) {
		
        // Create return Message
        Hashtable crypthash = createCryptHashtable();
        //HashSet idlist = (HashSet)crypthash.keySet();
        Set idlist = (Set)crypthash.keySet();
        //Iterator<String> iterator = idlist.iterator();
        Iterator<CharSequence> iterator = idlist.iterator();

        //String messageid = (String)idLong.toString();
        String messageid = _messageid;
        // Replace numbers with letters in Message
        while (iterator.hasNext()) {
            //String id = iterator.next();
            CharSequence idletter = iterator.next();
            // Check if id contains String 
            if (messageid.contains(idletter)) {
            	// Get Letter from HashSet and replace in String
            	String letter = (String)crypthash.get(idletter);
            	messageid = messageid.replaceAll(idletter.toString(), letter);
            }
            
        }
        return messageid;
	}


	/**
	 * Decode ID with letters and Numbers to numbers
	 * @param _messageid : id to decode with only numbers.
	 * @return : id with numbers.
	 */
	public static String iddecoding(String _messageid) {
	    // Create return Message
        Hashtable decrypthash = createDecryptHashtable();
        //HashSet idlist = (HashSet)crypthash.keySet();
        Set letterlist = (Set)decrypthash.keySet();
        //Iterator<String> iterator = idlist.iterator();
        //Collection letters = crypthash.values();
        Iterator<CharSequence> iteletters = letterlist.iterator();

        //String messageid = (String)idLong.toString();
        String messageid = _messageid;
        // Replace letters with numbers in Message
        while (iteletters.hasNext()) {
            //String id = iterator.next();
            CharSequence letter = iteletters.next();
            // Check if id contains String 
            if (messageid.contains(letter)) {
            	// Get Letter from HashSet and replace in String
            	String idletter = (String)decrypthash.get(letter);
            	messageid = messageid.replaceAll(letter.toString(), idletter.toString());
            }
            
        }
        return messageid;
	}
	
	
	
	private static Hashtable createCryptHashtable() {
		
		Hashtable crypthash = new Hashtable();
		
        crypthash.put("01", "A");
        crypthash.put("02", "B");
        crypthash.put("03", "C");
        crypthash.put("04", "D");
        crypthash.put("05", "E");
        crypthash.put("06", "F");
        crypthash.put("07", "G");
        crypthash.put("08", "H");
        crypthash.put("09", "I");
        crypthash.put("10", "J");
        crypthash.put("11", "K");
        crypthash.put("12", "L");
        crypthash.put("14", "M");
        crypthash.put("15", "N");
        crypthash.put("16", "O");
        crypthash.put("17", "P");
        crypthash.put("18", "Q");
        crypthash.put("19", "R");
        crypthash.put("20", "S");
        crypthash.put("21", "T");
        crypthash.put("22", "U");
        crypthash.put("23", "V");
        crypthash.put("24", "W");
        crypthash.put("25", "X");
        crypthash.put("26", "Y");
        crypthash.put("27", "Z");
        
        return crypthash;
        
	}
	
	
	private static Hashtable createDecryptHashtable() {
		
		Hashtable crypthash = new Hashtable();
		
        crypthash.put("A","01");
        crypthash.put("B","02");
        crypthash.put("C","03");
        crypthash.put("D","04");
        crypthash.put("E","05");
        crypthash.put("F","06");
        crypthash.put("G","07");
        crypthash.put("H","08");
        crypthash.put("I","09");
        crypthash.put("J","10");
        crypthash.put("K","11");
        crypthash.put("L","12");
        crypthash.put("M","14");
        crypthash.put("N","15");
        crypthash.put("O","16");
        crypthash.put("P","17");
        crypthash.put("Q","18");
        crypthash.put("R","19");
        crypthash.put("S","20");
        crypthash.put("T","21");
        crypthash.put("U","22");
        crypthash.put("V","23");
        crypthash.put("W","24");
        crypthash.put("X","25");
        crypthash.put("Y","26");
        crypthash.put("Z","27");
        
        return crypthash;
        
	}
	
	
}
