package com.miar.miarcrypt.business;

//import org.springframework.stereotype.Service;

import com.miar.miarcrypt.dto.Message;

//@Service
public class CryptBean implements Crypt {

	@Override
	public String crypt(Message message) {
		
		String encryptedMess =  "AZE123-" + message.getTextToCrypt() + "-456RTY";
		return encryptedMess;
	}

	@Override
	public String uncrypt(Message message) {
		return null;
	}

	@Override
	public String testFunction(Message message) {
		String encryptedMess =  "The textToCrypt is : " + message.getTextToCrypt() + " <br/> Key : " 
				+ message.getKey() 
				+ "<br/> The message is : " + message.getMessage();
				
				
				return encryptedMess;
	}

}
