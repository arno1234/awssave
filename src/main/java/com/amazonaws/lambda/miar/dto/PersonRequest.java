package com.amazonaws.lambda.miar.dto;

//import com.google.gson.Gson;
//import com.tuto.dto.Person;

public class PersonRequest {
	
    private String firstName;
    private String lastName;
	// Test DynamoDB
    private String message;
    private String key;
    
    
//    public PersonRequest(String json) {
//        Gson gson = new Gson();
//        PersonRequest request = gson.fromJson(json, PersonRequest.class);
//        this.id = request.getId();
//        this.name = request.getName();
//    }
    
    
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
    
    
    
    public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	

}
