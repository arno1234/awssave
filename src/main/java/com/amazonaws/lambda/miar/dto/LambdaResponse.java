package com.amazonaws.lambda.miar.dto;

public class LambdaResponse {
	
	
	private String isBase64Encoded ; 	// true|false
	private String statusCode; 			//"200, 400
	private String headers; 			// { "headerName": "headerValue", ... }
	private String multiValueHeaders;	// { "headerName": ["headerValue", "headerValue2", ...]
	private String body ; 
	

	public String getIsBase64Encoded() {
		return isBase64Encoded;
	}

	public void setIsBase64Encoded(String isBase64Encoded) {
		this.isBase64Encoded = isBase64Encoded;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getHeaders() {
		return headers;
	}

	public void setHeaders(String headers) {
		this.headers = headers;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getMultiValueHeaders() {
		return multiValueHeaders;
	}

	public void setMultiValueHeaders(String multiValueHeaders) {
		this.multiValueHeaders = multiValueHeaders;
	}
  

	
	
	
	

}
