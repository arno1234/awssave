package com.amazonaws.lambda.miar.save;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.lambda.miar.dto.PersonRequest;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
//import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.PutItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.util.IOUtils;
import com.miar.miarcrypt.service.EncodeSRVC;
import com.miar.miarcrypt.utils.CryptUtils;

//public class SavePersonHandler implements RequestHandler<PersonRequest, PersonResponse> {
//public class SavePersonHandler implements RequestHandler<PersonRequest, LambdaResponse> {
	
public class CrptHandler implements RequestStreamHandler {

	// DYNAMODB
	static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
	static DynamoDB dynamoDB = new DynamoDB(client);
	// TODO : use 2 tables ?
    private String DYNAMODB_TABLE_NAME_PERSON = "Person";
    private String DYNAMODB_TABLE_NAME_MESSAGE = "Message";
    private String DYNAMODB_TABLE_NAME_USER = "User";
    
    
    //private Regions REGION = Regions.EU_WEST_2;

    // XML Balise
    // TODO : add constants to static class
    private String JSON_REQUEST_TYPE = "requesttype";
    private String JSON_REQUEST_FIRSTNAME = "firstname";
    private String JSON_REQUEST_LASTNAME = "lastname";
    
    private String JSON_VALUE_REQUEST_TYPE_USER = "user";
    private String JSON_VALUE_REQUEST_TYPE_MESSAGE = "message";
    private String JSON_VALUE_REQUEST_TYPE_UNCRYPT = "uncrypt";
   
    //TODO : put this in external class.
    // JSON RESPONSE
    	// BALISES
    private  String RESPONSE_STATUSCODE = "statusCode";
    private  String RESPONSE_HEADERS = "headers";
    private  String RESPONSE_CUSTOMHEADERS = "x-custom-header";
    private  String RESPONSE_ACCESSCONTROL = "Access-Control-Allow-Origin";
    private  String RESPONSE_BODY = "body";
    private  String RESPONSE_MESSAGE = "message";
    	// VALUES
    private String JSON_Response_statuscode = "200";
    private String JSON_Response_headers = "";
    private String JSON_Response_customheaders = "MIARCRYPT custom header value";
    private String JSON_Response_AccessCotrol = "*";    
    private String JSON_Response_body = "";
    private String JSON_Response_Message = "";    
    
//    headerJson.put("x-custom-header", "ARNO custom header value");
//    headerJson.put("Access-Control-Allow-Origin", "*");
//    responseJson.put("statusCode", 200);
//    responseJson.put("headers", headerJson);
//    responseJson.put("body", responseBody.toString());
    
	@Override
	public void handleRequest(
			  InputStream inputStream, 
			  OutputStream outputStream, 
			  Context context)
			  throws IOException {
		
		// Prepare JSON Response
		JSONObject responseJson = new JSONObject();
		
        
		//AmazonDynamoDB client = AmazonDynamoDBClientBuilder.defaultClient();
	    this.initDynamoDbClient();
	    DynamoDB dynamoDb = new DynamoDB(client);

	    // Process JSON Request
	    JSONParser parser = new JSONParser();
	    System.out.println("JSONParser parser : " + parser.toString());	
	    
	    
        System.out.println("inputStream : " + inputStream.toString());	        
        String text = IOUtils.toString(inputStream);
        System.out.println("TEXT received in inputstream : " + text);
        
	    try {
	    
	    	System.out.println("Before Event - no use of reader ");
	        JSONObject event = (JSONObject) parser.parse(text);
	        System.out.println("Event : " + event.toJSONString());	   

	        //if (event.get("user") != null) {
	        if (event.get(JSON_REQUEST_TYPE) != null) {	
	        	// Response
	        	//JSONObject responseBody = new JSONObject();
	        	
	        	String requestType = (String)event.get(JSON_REQUEST_TYPE);
		        System.out.println("Has JSON_REQUEST_TYPE : "+requestType);
		        
	        	Instant instant = Instant.now();
		    	long timeStampMillis = instant.toEpochMilli();
		    	
//		    	// Instanciate sent message var
//		    	String messageString = "";
//		    	byte[] message = messageString.getBytes();
	        	
			        	// Process JSON request
			        	if (requestType.equalsIgnoreCase(JSON_VALUE_REQUEST_TYPE_USER)) {
			        		
			        		// SAVE NEW USER
			        		System.out.println("Save new USER");	  
			        		
					    	
					    	String firstname = (String)event.get(JSON_REQUEST_FIRSTNAME);
					    	String lastname = (String)event.get(JSON_REQUEST_LASTNAME);
					    	
					    	

				            //dynamoDb.getTable(DYNAMODB_TABLE_NAME_USER)
					    	dynamoDb.getTable(DYNAMODB_TABLE_NAME_USER)
				              .putItem(new PutItemSpec().withItem(new Item().withNumber("id", timeStampMillis)
				            		  .withString(JSON_REQUEST_FIRSTNAME, firstname)	
				            		  .withString(JSON_REQUEST_LASTNAME, lastname)));
			        		
					    	
				            JSON_Response_Message="New USER - SAVED";
				            JSON_Response_statuscode = "200";
				            
			        	}else if(requestType.equalsIgnoreCase(JSON_VALUE_REQUEST_TYPE_MESSAGE)){
			        		
			        			// Crypt MESSAGE and save in DB
			        			System.out.println("Save new MESSAGE");	  
			        			
			    		    	// Instanciate sent message var
			    		    	//String messageString = "";
			    		    	//byte[] message = messageString.getBytes();

			        			
						    	//String message = (String)event.get("message");
						    	//messageString = (String)event.get("message");
			    		    	String messageString = (String)event.get("message");
			    		    	byte[] message = messageString.getBytes();
			    		    	
						    	System.out.println("Message in event : " + message);
						    	String key = (String)event.get("key");
						    	System.out.println("Key in event : " + key);
						    	
						    	// Crypt Message
						    	//message = cryptMessage(messageString);
						    	message = cryptMessage(messageString, key);
						    	System.out.println("Message Crypted : " + message);
						    	
						    	// TODO : save ONLY Binary part in DB
					            dynamoDb.getTable(DYNAMODB_TABLE_NAME_MESSAGE)
					              .putItem(new PutItemSpec().withItem(new Item().withNumber("id", timeStampMillis)
					            		  .withBinary("binarymess", message)
					            		  .withString("key", key)
					            		  .withNumber("view", 0)));
					            // .withString("message", messageString)
					            
					            // ENCODE MESSAGE ID before sending to user
					            Long idLong = timeStampMillis;
					            String messageid = (String)idLong.toString();
					            messageid = CryptUtils.idencoding(messageid);
					            //responseBody.put("crypted", timeStampMillis);
					            //JSON_Response_Message="CRYPTED MESSAGE : "+ timeStampMillis;
					            // Add Return with new ID 
					            //JSON_Response_Message="CRYPTED MESSAGE : "+ messageid;
					            JSON_Response_Message=messageid;
					            JSON_Response_statuscode = "200";
			        		
			        	}else if(requestType.equalsIgnoreCase(JSON_VALUE_REQUEST_TYPE_UNCRYPT)){
			        		
		        			// UNCrypt MESSAGE saved in DB
		        			System.out.println("UNCRYPT MESSAGE");	  
					    	
					    	JSONObject requestPersonJSON = (JSONObject) parser.parse(event.toJSONString());
					    	String messageid = (String)event.get("message");
					    	String key = (String)event.get("key");
					    	System.out.println("MessageID in event : " + messageid);
					    	System.out.println("Key in event : " + key);
							 
					    	// Decode Message ID
					    	messageid = CryptUtils.iddecoding(messageid);
					    	System.out.println("MessageID after decrypt : " + messageid);
					    	
					    	// Get Binary Message from DB
					    	HashMap<String,AttributeValue> key_to_get = new HashMap<String,AttributeValue>();
					    	//key_to_get.put("DATABASE_NAME", new AttributeValue(DYNAMODB_TABLE_NAME));
					    	key_to_get.put("id", new AttributeValue(messageid));
					    	
					    	System.out.println("GET TABLE");
					    	// MESSAGE : 1587902722058 / 1587919856832 - Crypted Part
					    	// USER : 1587734453676
					    	Table table = dynamoDB.getTable(DYNAMODB_TABLE_NAME_MESSAGE);
					    	System.out.println("TABLE : "+table.getTableName());
					    	System.out.println("messageid :"+messageid);
					    	Long idmess = Long.valueOf(messageid.trim());
					    	Item item = table.getItem("id", idmess);
					    	
					    	
						    	if (item == null) {
						    		System.out.println("RETRIEVED ITEM NULL");
						    		// TODO : manage Error Codes
						    		JSON_Response_statuscode = "400";
						    		JSON_Response_Message="CRYPTED MESSAGE DOES NOT EXIST";
						    		
						    	}else {
						    		
						    		System.out.println("RETRIEVED ITEM : " +item.toJSONPretty()) ;
						    		
						    		// Check is Key is ok
						    		String keydb = (String)item.get("key");
						    		// if Key is Ok, we can uncrypt
						    		if (keydb.equalsIgnoreCase(key)) {
						    		
						    				// KEY is OK / Uncrypt Binary MESSAGE
								    		Object binarymessage = item.get("binarymess");
								    		System.out.println("Object getClass : "+binarymessage.getClass());
								    		System.out.println("Object toString : "+binarymessage.toString());
								    		
								    		byte[] binMess = (byte[])binarymessage;
								    		System.out.println("Object to BYTE  : "+binMess.toString());
								    		
								    		// UNCRYPT MESSAGE RETRIEVED FROM DB
								    		try {
								    			// TEST Datas : for userid : 2705123
								    			// TODO : delete Key when Decrypt because it's already in constructor.
								    			EncodeSRVC service = new EncodeSRVC(keydb, 2705123);
												
												String uncryptedmessage = service.decrypt(keydb, binMess);
												System.out.println("Uncrypted Message :" +uncryptedmessage);

									    		// Update DB to register that message has been decrypted
												// Get Number of message view
									    		BigDecimal viewnumber =  (BigDecimal)item.get("view");
									    		int messageview = viewnumber.intValue();
									    		messageview++;

									    		UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("id", item.get("id"))
										                .withUpdateExpression("set #na = :val1").withNameMap(new NameMap().with("#na", "view"))
										                .withValueMap(new ValueMap().withNumber(":val1", messageview)).withReturnValues(ReturnValue.ALL_NEW);
												UpdateItemOutcome outcome = table.updateItem(updateItemSpec);
												
												JSON_Response_Message=uncryptedmessage;
												JSON_Response_statuscode = "200";
												
											} catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
												JSON_Response_statuscode = "400";
												System.out.println("Exception when UNCODING message : \" + e.getMessage()");
												JSON_Response_Message = "PROBLEM while UNCODING !";
											}
						    		}else {
						    			// KEY is not OK
						    			JSON_Response_statuscode = "400";
										JSON_Response_Message = "The Key is NOT CORRECT for this message ";
						    		}
						    	}  
					    	
		        	}else {
			        		
			        		System.out.println("UNKNOWN requesttype : " +(String)event.get(JSON_REQUEST_TYPE));
			        		JSON_Response_Message="STOP Playing with us...";
			        		JSON_Response_statuscode = "400";
			        }
		        	

			    // Create JSON Response 		
		        JSONObject responseBody = new JSONObject();
		        responseBody.put("message", JSON_Response_Message );
		        //responseBody.put("crypted", "jkjjHg6è_è5THbjh6-mm");
		        //responseBody.put("crypted", message);
		        JSONObject headerJson = new JSONObject();
		        headerJson.put("x-custom-header", "ARNO custom header value");
		        headerJson.put("Access-Control-Allow-Origin", "*");
		 
		        //responseJson.put("statusCode", 200);
		        responseJson.put("statusCode", JSON_Response_statuscode);
		        responseJson.put("headers", headerJson);
		        responseJson.put("body", responseBody.toString());
	        
	        }else {
	        	
		        JSONObject responseBody = new JSONObject();
		        System.out.println("CryptHandler Request Error : requesttype is missing " + text) ;
		        responseBody.put("message", "STOP Playing with us...");
		 
		        JSONObject headerJson = new JSONObject();
		        headerJson.put("x-custom-header", "ARNO custom header value");
		        headerJson.put("Access-Control-Allow-Origin", "*");
		       
		        responseJson.put("statusCode", 400);
		        responseJson.put("headers", headerJson);
		        responseJson.put("body", responseBody.toString());
	        	
	        } 
	 
	    } catch (ParseException pex) {
	    	
	    	System.out.println("ParseException : " + pex.getMessage());	
	        responseJson.put("statusCode", 500);
	        responseJson.put("ERROR : ", pex);
	    
	    }
	    
	    // SEND JSON RESPONSE 
	    OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
	    writer.write(responseJson.toString());
	    writer.close();
	}
	
	
	   private void initDynamoDbClient() {
		   // Paris : UE_WEST_3
		   AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
	        		.withRegion(Regions.EU_WEST_3)
	        		.build();  

	        this.client =  client;
	    }
	
	   // TODO : add table name as parameter and us it in Handler
	    private PutItemOutcome persistData(PersonRequest personRequest, String tablename) throws ConditionalCheckFailedException {
	    	
	    	//Table table = dynamoDB.getTable(DYNAMODB_TABLE_NAME_MESSAGE);
	    	Table table = dynamoDB.getTable(tablename);
	    	
	    	String name = personRequest.getFirstName() + " " + personRequest.getLastName();
	    	String message = personRequest.getMessage() + " " + personRequest.getKey();
	    	
	    	Instant instant = Instant.now();
	    	long timeStampMillis = instant.toEpochMilli();
	    	
	    	Item item = new Item().withString("message", message)
	    						.withLong("id", timeStampMillis);
	    	
	        return table.putItem(item);
	    }	  


	    
	    /**
	     * Encrypt data with Crypting Service.	
	     * @param pmessage : string to encrypt.
	     * @param pkey : key to encrypt the message.
	     * @return encrypted String.
	     */
		//private String cryptMessage(String pmessage) {
	    private byte[] cryptMessage(String pmessage, String pkey) {

		   //String encryptedmessage = null ;
		   byte[] encryptedmessage = null ;
		   
		   System.out.println("BEFORE Crypting"); 
		   System.out.println("Message to crypt : "+pmessage);
		   System.out.println("Key to crypt : "+pkey);
		   
		   
		   try {
				   //String returnMessage = pmessage;
				   //return "159" + returnMessage + "753";
				   // Encrypt Message
				   // TODO : add logged user ID as long parameter
				   //EncodeSRVC service = new EncodeSRVC(pmessage, 159753);
				   // TODO : there is no logged user for the moment. Logged user is a constant ID
			   	   EncodeSRVC service = new EncodeSRVC(pkey, 159753);
				   System.out.println("EncodeSRVC : "+service.toString());
				   encryptedmessage = service.encrypt(pmessage);
				   
			}catch(Exception e) {
				
				System.out.println("EXCEPTION : " +e.getMessage()); 
			}
			
			return encryptedmessage;
			   
		}

	    
	    
//	    /**
//	     * Encrypt data with Crypting Service.	
//	     * @param pmessage : string to encrypt.
//	     * @return encrypted String.
//	     */
//		//private String cryptMessage(String pmessage) {
//	    private byte[] cryptMessage(String pmessage) {
//
//		   //String encryptedmessage = null ;
//		   byte[] encryptedmessage = null ;
//		   
//		   System.out.println("BEFORE Crypting"); 
//		   System.out.println("Message to crypt : "+pmessage);
//			try {
//				   //String returnMessage = pmessage;
//				   //return "159" + returnMessage + "753";
//				   // Encrypt Message
//				   // TODO : add logged user ID as long parameter
//				   //EncodeSRVC service = new EncodeSRVC(pmessage, 159753);
//				   EncodeSRVC service = new EncodeSRVC("miarcryptkey", 159753);
//				   System.out.println("EncodeSRVC : "+service.toString());
//				   encryptedmessage = service.encrypt(pmessage);
//				   
//			}catch(Exception e) {
//				
//				System.out.println("EXCEPTION : " +e.getMessage()); 
//			}
//			
//			return encryptedmessage;
//			   
//		}
	    

}
