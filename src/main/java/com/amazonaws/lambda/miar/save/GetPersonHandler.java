package com.amazonaws.lambda.miar.save;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.lambda.miar.dto.PersonResponse;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.dynamodbv2.model.GetItemResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class GetPersonHandler implements RequestHandler<String, PersonResponse> {

	static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
	static DynamoDB dynamoDB = new DynamoDB(client);

    private String DYNAMODB_TABLE_NAME = "Person";
    private String DYNAMODB_PERSON_ID = "id";
    
    //private Regions REGION = Regions.EU_WEST_2;
	
	@Override
	public PersonResponse handleRequest(String input, Context context) {
		   this.initDynamoDbClient();
		   
		   requestData(input);
	 
	        PersonResponse personResponse = new PersonResponse();
	        personResponse.setMessage("Get Successfully!!!");
	        return personResponse;
	}
    
    
	   private void initDynamoDbClient() {
		   AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
	        		.withRegion(Regions.EU_WEST_2)
	        		.build();  
		   
		   this.client =  client;
	    }

	
	    private GetItemResult requestData(String idPersonRequest) throws ConditionalCheckFailedException {
	    	
		    final AmazonDynamoDB ddb = AmazonDynamoDBClientBuilder.defaultClient();
	    	
	    	GetItemRequest request = null;
		    HashMap<String,AttributeValue> keyDataToGet = new HashMap<String,AttributeValue>();
		    
		    AttributeValue paramId = new AttributeValue(idPersonRequest);
		    
		    keyDataToGet.put(DYNAMODB_PERSON_ID, paramId);
		    keyDataToGet.put("name", new AttributeValue("Bernard Smith"));
		    
		    Map<String,AttributeValue> itemToReturn = null;
		    
		    request = new GetItemRequest()
		            .withKey(keyDataToGet)
		            .withTableName(DYNAMODB_TABLE_NAME);
		    
		    try {
		    	
		        Map<String,AttributeValue> returned_item = ddb.getItem(request).getItem();
		        if (returned_item != null) {
		            Set<String> keys = returned_item.keySet();
		            for (String key : keys) {
		                System.out.format("%s: %s\n",
		                        key, returned_item.get(key).toString());
		            }
		            
		        } else {
		            System.out.format("No item found with the ID  %s!\n", idPersonRequest);
		        }
		        
		        itemToReturn=returned_item;
		        
		    } catch (AmazonServiceException e) {
		        System.err.println(e.getErrorMessage());
		        //System.exit(1);
		    }	
		    
		    return (GetItemResult)itemToReturn;
	    	        
	  }	  


	    

}
