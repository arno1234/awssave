package com.miar.miarcrypt.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.miar.miarcrypt.utils.CryptUtils;

import junit.framework.TestCase;

public class EncodeSRVCTest extends TestCase {
	
	static EncodeSRVC service;
	static String outputAttribute;
	
	static byte[] bytesToEncrypt = null;
	static byte[] encryptedBytes = null;
	
	//@BeforeClass
    @Before
    public static void initService() throws IOException {
    	// 159753
    	long userId = 159753;
    	try {
			service = new EncodeSRVC("12345678", userId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	
    
    @Test
    public void testCrypt() {
    	
    	// Test : [B@2fc14f68 / Key : miarcryptkey
    	// Init Service
    	try {
			initService();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	String stringToEncrypt = "This is a JUnit test";
    	//String output = service.encrypt(stringToEncrypt);
    	byte[] output = service.encrypt(stringToEncrypt);
    	System.out.println("Output Bytes Service Crypt : " + output);
    	encryptedBytes = output;
    	
    	outputAttribute = output.toString();
    	System.out.println("Attribute Output Service Crypt : " + outputAttribute);
    	System.out.println("Attribute encryptedBytes Service Crypt : " + encryptedBytes);
    	
    	Assert.assertEquals("", output);
      
    }

    
    @Test
    public void testDecrypt() {
    	
    	// Test : [B@2fc14f68 / Key : miarcryptkey
    	// Init Service
    	try {
			initService();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	System.out.println("Attribute encryptedBytes Service Crypt : " + encryptedBytes);
    	//outputAttribute = "[B@21a947fe";
    	//String output = service.decrypt("miarcryptkey", "[B@2fc14f68");
    	String output = service.decrypt("12345678", encryptedBytes);
    	System.out.println("Output Service Decrypt : " + output);
    	
    	// Test FER
    	/**
    	 output = service.decrypt("1590002170753", encryptedBytes);
    	System.out.println("Output Service Decrypt : " + output);
    	
    	 output = service.decrypt("1590072372836", encryptedBytes);
    	System.out.println("Output Service Decrypt : " + output);
    	
		*/
    	
    	Assert.assertEquals("", output);
      
    }
	
    @Test
    public void testEncodeId() {
    	
    	System.out.println("OUTPUT EncodeID : " + CryptUtils.idencoding("1588261775691"));
    	
    	System.out.println("OUTPUT EncodeID : " + CryptUtils.idencoding("1588877947018"));
    	
    	System.out.println("OUTPUT EncodeID : " + CryptUtils.idencoding("1588261847900"));
    	
    	System.out.println("OUTPUT EncodeID : " + CryptUtils.idencoding("1589636197497"));
    	      
    }
	
    
    
    @Test
    public void testDecodeId() {
    	
    	System.out.println("OUTPUT DEcodeID : " + CryptUtils.iddecoding("N88YP75691"));
    	
    	System.out.println("OUTPUT DEcodeID : " + CryptUtils.iddecoding("N88877947A8"));
    	
    	System.out.println("OUTPUT DEcodeID : " + CryptUtils.iddecoding("N88879EL44"));
    	
    	System.out.println("OUTPUT DEcodeID : " + CryptUtils.iddecoding("N88YQ47900"));
    	      
    }
    
    
    
 /*   
    @Test
    public void testDecryptMessage() {
    	
    	// Test : [B@2fc14f68 / Key : miarcryptkey
    	// Init Service
    	try {
			initService();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	System.out.println("Attribute encryptedBytes Service Crypt : " + encryptedBytes);
    	//outputAttribute = "[B@21a947fe";
    	//String output = service.decrypt("miarcryptkey", "[B@2fc14f68");
    	String outputTest = new String("[B@2fc14f68"); 
    	try {
			encryptedBytes = outputTest.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	String output = service.decrypt("12345678", encryptedBytes);
    	System.out.println("Output Service Decrypt : " + output);

    	Assert.assertEquals("", output);
      
    }
  */  

}
