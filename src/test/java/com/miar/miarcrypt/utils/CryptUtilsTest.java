package com.miar.miarcrypt.utils;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.miar.miarcrypt.service.EncodeSRVC;

import junit.framework.TestCase;

public class CryptUtilsTest extends TestCase {

	static CryptUtils utils = new CryptUtils();
	
	 @BeforeClass
	 public static void initUtils() throws IOException {
		 
		 utils = new CryptUtils();
	 }
	
	
    @Test
    public void testIddecoding() {
	    	
    	String encodedId = "E123Z" ;
    	String decodedId = "";
    	try {
			
    		decodedId = utils.iddecoding(encodedId);
    		System.out.println("Output decoded ID : " + decodedId);
    		
		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	
		Assert.assertEquals("0512327", decodedId);
	    
	 }	 
	
	
    /**
     * Test to print encoded ID : from numerical ID to id with letters
     */
    @Test
    public void testIdEncoding() {
	    	
    	String decodedId = "";
    	String encodedId = "";
    	try {
			
    		decodedId = "1590244923089" ;
    		encodedId = utils.idencoding(decodedId);
    		// Test BRASIL
    		System.out.println("Output encoded ID 1590244923089 : " + encodedId);
    		
    		decodedId = "1590072372836" ;
    		encodedId = utils.idencoding(decodedId);
    		System.out.println("Output encoded ID 1590072372836 : " + encodedId);
 
    		
		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	
    	// Only to have green test
		Assert.assertTrue(true);
	    
	 }
    
    //

    /**
     * Test to print encoded ID : from numerical ID to id with letters
     */
    @Test
    public void testEncoding() {
	    	
    	String decodedId = "1591530622042";
    	String encodedId = "";
    	try {
			
    		//decodedId = "1590002170753" ;
    		encodedId = utils.idencoding(decodedId);
    		
    		// Test FER
    		System.out.println("Output encoded ID "+decodedId+" : " + encodedId);
//    		decodedId = "1590072372836" ;
//    		encodedId = utils.idencoding(decodedId);
//    		System.out.println("Output encoded ID 1590072372836 : " + encodedId);
 
    		
		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	
    	// Only to have green test
		Assert.assertTrue(true);
	    
	 }
    
	
}
